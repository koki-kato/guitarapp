FactoryBot.define do
  factory :user, class: 'User' do
    id               { 1 }
    name             { 'test' }
    email            { 'test@example.com' }
    password         { 'password' }
  end
end